// Package gtkloop provides an API wrapper around C calls to GTK.  The functions
// provided are designed to match the operations required by bitbucket.org/rj/goey/loop,
// and are not intended for general use.
package gtkloop
