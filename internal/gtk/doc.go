// Package gtk provides an API wrapper around C calls to GTK.  The functions
// provided are designed to match the operations required by bitbucket.org/rj/goey,
// and are not intended for general use.
package gtk
